﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    [SerializeField] Transform parentTransform;

    // Transform of the GameObject you want to shake
    private Transform transform;

    // Desired duration of the shake effect
    private float shakeDuration = 0f;

    // A measure of magnitude for the shake. Tweak based on your preference
    private float shakeMagnitude = 0.7f;

    // A measure of how quickly the shake effect should evaporate
    private float dampingSpeed = 1.0f;

    // The initial position of the GameObject
    Vector3 initialPosition;

    // Update is called once per frame

    void Awake()
    {
        if (transform == null)
        {
            transform = GetComponent(typeof(Transform)) as Transform;
        }
    }

    private void Start()
    {
        initialPosition = new Vector3(transform.position.x,transform.position.y,-10);
    }

    void Update()
    {


        if (shakeDuration > 0)
        {
            transform.position = initialPosition + Random.insideUnitSphere * shakeMagnitude;
            shakeDuration -= Time.deltaTime * dampingSpeed;
        }
        else
        {
            shakeDuration = 0f;
            initialPosition = new Vector3(parentTransform.position.x, parentTransform.position.y, -10);
            transform.position = initialPosition;
        }
    }

    public void TriggerShake(float duration,float magnitude)
    {
        shakeDuration = duration;
        shakeMagnitude = magnitude;
        initialPosition = new Vector3(transform.position.x,transform.position.y,-10);
    }
}
