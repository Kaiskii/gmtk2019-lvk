﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    public bool IS_IN_TUTORIAL = true;

    [SerializeField] GameObject slotHighlights;
    [SerializeField] SpriteRenderer chipHighlight;
    [SerializeField] ModuleManager modules;
    [SerializeField] PlugChip chip;

    [SerializeField] Typewriter topText;
    [SerializeField] Text topTextRaw;
    [SerializeField] Typewriter bottomText;
    [SerializeField] Text bottomTextRaw;

    Modules currentModule;

    float counter = 0;

    bool Has_Text_Init = false;
    bool Has_Higlighted_Chip = false;
    bool Has_Held_Chip = false;
    bool Has_Socketed_Chip = false;

    void Start()
    {
        chipHighlight.enabled = false;
        slotHighlights.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (IS_IN_TUTORIAL)
        {
            modules.activeModule = Modules.Tutorial;
            counter += Time.deltaTime;

            if (counter >= 2 && !Has_Text_Init)
            {
                topText.Play = true;
                bottomText.Play = true;

                Has_Text_Init = true;
            }

            if (counter >= 3 && !Has_Higlighted_Chip)
            {
                chipHighlight.enabled = true;
                Has_Higlighted_Chip = true;
            }

            if (chip.held == true && !Has_Held_Chip)
            {
                bottomText.InterruptPlaying();

                slotHighlights.SetActive(true);
                chipHighlight.enabled = false;
                bottomText.story = "place chip in indicated slots";
                bottomText.Play = true;
                Has_Held_Chip = true;
            }

            if (chip.slotted == true && !Has_Socketed_Chip)
            {
                bottomText.InterruptPlaying();

                slotHighlights.SetActive(false);
                chipHighlight.enabled = true;
                bottomText.story = "drag chip to remove it from slot";
                bottomText.Play = true;
                Has_Socketed_Chip = true;
            }

            if (chip.slotted == false && Has_Socketed_Chip)
            {
                bottomText.InterruptPlaying();
                topText.InterruptPlaying();

                chipHighlight.enabled = false;
                bottomText.story = "welcome aboard, captain";
                bottomText.Play = true;
                topText.story = "INITIALIZATION COMPLETE";
                topText.Play = true;
                Has_Socketed_Chip = false;
                IS_IN_TUTORIAL = false;
            }
        }
        else
        {
            if (currentModule != modules.activeModule)
            {
                currentModule = modules.activeModule;

                if (!Has_Held_Chip)
                {
                    topText.InterruptPlaying();
                    bottomText.InterruptPlaying();
                }
                else
                    Has_Held_Chip = false;



                if (currentModule == Modules.Navigation)
                {
                    topTextRaw.color = Color.yellow;
                    bottomTextRaw.color = Color.yellow;

                    topText.story = "NAVIGATION";
                    topText.Play = true;
                    bottomText.story = "[left mouse] to place waypoint - [shift + left mouse] to queue waypoints";
                    bottomText.Play = true;
                }
                else if (currentModule == Modules.Weapons)
                {
                    topTextRaw.color = Color.red;
                    bottomTextRaw.color = Color.red;

                    topText.story = "WEAPONS";
                    topText.Play = true;
                    bottomText.story = "[left mouse] to fire torpedo at mouse cursor";
                    bottomText.Play = true;
                }
                else if (currentModule == Modules.Information)
                {
                    topTextRaw.color = Color.green;
                    bottomTextRaw.color = Color.green;

                    topText.story = "INFORMATION";
                    topText.Play = true;
                    bottomText.story = "sonar and threat display - find a way home";
                    bottomText.Play = true;
                }
                else
                {
                    topTextRaw.color = Color.white;
                    bottomTextRaw.color = Color.white;

                    topText.story = "";
                    topText.Play = true;
                    bottomText.story = "";
                    bottomText.Play = true;
                }
            }
        }
    }
}
