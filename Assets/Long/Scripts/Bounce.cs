﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour
{
    public bool isEnemy = false;

    public float delta = 1.5f;  // Amount to move left and right from the start point
    public float speed = 2.0f;
    public Transform parentPos;
    private Vector3 startPos;

    void Start()
    {
        if (isEnemy)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            delta = Random.Range(1.0f,2.5f);
        }

        if (parentPos != null)
            startPos = transform.position + parentPos.transform.position;
        else
            startPos = transform.position;
    }

    void Update()
    {
        Vector3 v = startPos;
        v.y += delta * Mathf.Sin(Time.time * speed);

        if (parentPos != null)
            transform.position = v + parentPos.transform.position;
        else
            transform.position = v;
    }
}

