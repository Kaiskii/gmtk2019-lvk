﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBars : MonoBehaviour
{
    [SerializeField] ModuleManager manager;
    Modules currentModule = Modules.None;
    [SerializeField] SpriteRenderer chipLight;

    // Update is called once per frame
    void Update()
    {
        if (currentModule != manager.activeModule)
        {
            currentModule = manager.activeModule;

            if (manager.activeModule == Modules.Navigation)
            {
                chipLight.color = new Color(200, 200, 0, chipLight.color.a);
                foreach (Transform childSprite in transform)
                {
                    SpriteRenderer rend = childSprite.GetComponent<SpriteRenderer>();
                    rend.color = new Color(200, 200, 0, rend.color.a);
                }
            }

            else if (manager.activeModule == Modules.Weapons)
            {
                chipLight.color = new Color(200, 0, 0, chipLight.color.a);
                foreach (Transform childSprite in transform)
                {
                    SpriteRenderer rend = childSprite.GetComponent<SpriteRenderer>();
                    rend.color = new Color(200, 0, 0, rend.color.a);
                }
            }

            else if (manager.activeModule == Modules.Information)
            {
                chipLight.color = new Color(0, 200, 0, chipLight.color.a);
                foreach (Transform childSprite in transform)
                {
                    SpriteRenderer rend = childSprite.GetComponent<SpriteRenderer>();
                    rend.color = new Color(0, 200, 0, rend.color.a);
                }
            }

            else if (manager.activeModule == Modules.Reload)
                chipLight.color = new Color(200, 0, 0, chipLight.color.a);

            else if (manager.activeModule == Modules.LifeSupport)
                chipLight.color = new Color(25, 200, 200, chipLight.color.a);

            else if (manager.activeModule == Modules.None)
            {
                foreach (Transform childSprite in transform)
                {
                    chipLight.color = new Color(0.2f, 0.2f, 0.2f, chipLight.color.a);
                    SpriteRenderer rend = childSprite.GetComponent<SpriteRenderer>();
                    rend.color = new Color(0.2f, 0.2f, 0.2f, rend.color.a);
                }
            }
        }
    }
}
