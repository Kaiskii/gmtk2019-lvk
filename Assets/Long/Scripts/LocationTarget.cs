﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationTarget : MonoBehaviour
{
    [SerializeField]Transform target;

    // Update is called once per frame
    void Update()
    {
        transform.right = target.position - transform.position;
    }
}
