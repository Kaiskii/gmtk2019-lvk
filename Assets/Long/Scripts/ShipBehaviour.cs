﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBehaviour : MonoBehaviour
{
    public CameraShake camera;

    public Vector2 BoundingBox;
    [SerializeField] GameObject world;
    [SerializeField] GameObject chip;
    [SerializeField] GameObject subHull;
    [SerializeField] Sprite Hull_Navigation;
    [SerializeField] Sprite Hull_Combat;
    [SerializeField] Sprite Hull_Sonar;
    [SerializeField] Sprite Hull_None;
    SpriteRenderer subSprite;

    [SerializeField] bool DEBUG_MODE = false;
    [SerializeField] ModuleManager ModMan;
    public Modules activeModule = Modules.None;

    [SerializeField] bool EngineOn = false;
    [SerializeField] bool NavigationOn = false;
    [SerializeField] bool WeaponsOn = false;
    [SerializeField] bool ReloadOn = false;
    [SerializeField] bool LifeSupportOn = false;

    //ENGINES
    public float maxSpeed = 2;
    [SerializeField] float acceleration = 0.1f;
    [SerializeField] float deceleration = 5f;

    public float shipSpeed;
    [SerializeField] IndicatorBar SpeedIndicator;

    //OXYGEN
    public int maxOxygen = 100;
    [SerializeField] float oxygenDepletionRate = 0.1f;
    [SerializeField] float oxygenRefillRate = 5f;

    public float oxygenLevel;
    [SerializeField] IndicatorBar OxygenIndicator;
    [SerializeField] SpriteRenderer OxygenLowIndicator;
    float oxygenWarningCounter;

    //WEAPONS
    [SerializeField] GameObject subTurret;
    [SerializeField] GameObject torpedo;
    [SerializeField] GameObject particleHolder;

    public float ammoLevel = 5;
    public float reloadCounter = 0;
    [SerializeField] IndicatorBar AmmoIndicator;
    [SerializeField] IndicatorBar ReloadIndicator;

    //NAVIGATION
    [SerializeField] GameObject targetPoint;
    List<GameObject> targetList = new List<GameObject>();
    [SerializeField] GameObject TargetPointer;

    LineRenderer lineRend;

    //HEALTH
    [SerializeField] GameObject cracks1;
    [SerializeField] GameObject cracks2;
    [SerializeField] GameObject cracks3;
    public float hitsTaken = 0;



    private void Start()
    {
        lineRend = GetComponent<LineRenderer>();
        subSprite = subHull.GetComponent<SpriteRenderer>();

        cracks1.SetActive(false);
        cracks2.SetActive(false);
        cracks3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (hitsTaken == 1)
            cracks1.SetActive(true);

        else if (hitsTaken == 2)
            cracks2.SetActive(true);

        else if (hitsTaken == 3)
            cracks3.SetActive(true);

        else if (hitsTaken == 4)
        {
            FadeInOut.Instance.isFadeIn = true;
            FadeInOut.Instance.sceneInt = 0;
        }

            //DIE


        if (!DEBUG_MODE)
        {
            /*
            if (activeModule == Modules.Engine)
                EngineOn = true;
            else
                EngineOn = false;
            */

            if (activeModule == Modules.Navigation)
            {
                subSprite.sprite = Hull_Navigation;
                NavigationOn = true;
                EngineOn = true;
            }
            else
            {
                NavigationOn = false;
                EngineOn = false;
            }

            if (activeModule == Modules.LifeSupport)
                LifeSupportOn = true;
            else
                LifeSupportOn = false;

            if (activeModule == Modules.Weapons)
            {
                subSprite.sprite = Hull_Combat;
                WeaponsOn = true;
                subTurret.SetActive(true);
            }
            else
            {
                WeaponsOn = false;
                subTurret.SetActive(false);
            }

            if (activeModule == Modules.Reload)
            {
                subSprite.sprite = Hull_Combat;
                ReloadOn = true;
            }
            else
                ReloadOn = false;

            if (activeModule == Modules.Information)
            {
                subSprite.sprite = Hull_Sonar;
                TargetPointer.SetActive(true);
            }
            else
                TargetPointer.SetActive(false);

            if (activeModule == Modules.None)
            {
                subSprite.sprite = Hull_None;
            }
        }

        //Run components
        RunEngine(EngineOn);
        RunNavigation(NavigationOn);
        RunLifeSupport(LifeSupportOn);

        UpdateIndicators();

        float step = shipSpeed * Time.deltaTime;

        //IF THERE ARE POINTS, MOVE TO THEM
        if (targetList.Count > 0)
        {
            Vector3 diff = targetList[0].transform.position - transform.position;

            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            Quaternion newRotation = Quaternion.Euler(0f, 0f, rot_z - 90);

            transform.rotation = Quaternion.Lerp(transform.rotation, newRotation,Time.deltaTime*10.0f);

            //While we're not at a point yet, move towards it
            //Debug.Log(Vector3.Distance(subHull.transform.position, targetList[0].transform.position));
            if (Vector3.Distance(subHull.transform.position, targetList[0].transform.position) > 0.2f)
            {
                //Move in the direction currently looked at
                world.transform.position += transform.up*step;
            }
            else
            {
                //Destroy the gameobject and remove it from the list once we've reached it
                GameObject temp = targetList[0];
                Destroy(temp);
                targetList.Remove(targetList[0]);
            }
        }

        //FLIP SUB SPRITE
        if (transform.rotation.eulerAngles.z > 180)
            subSprite.flipX = false;
        else
            subSprite.flipX = true;

        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);

        //POINT AT THING
        if (WeaponsOn)
        {

            Vector3 worldpos = Camera.main.ScreenToWorldPoint(mousePos);
            // Get Angle in Radians
            float AngleRad = Mathf.Atan2(worldpos.y - subTurret.transform.position.y, worldpos.x - subTurret.transform.position.x);
            // Get Angle in Degrees
            float AngleDeg = (180 / Mathf.PI) * AngleRad;
            // Rotate Object
            subTurret.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
        }

        //RELOAD
        if(ReloadOn && ammoLevel < 5)
        {
            reloadCounter += Time.deltaTime;

            if (reloadCounter >= 2)
            {
                ammoLevel++;
                reloadCounter = 0;
            }
        }

        if (Input.GetMouseButtonDown(0) && GetComponent<CircularScan>().CheckInBoundingBox(Camera.main.ScreenToWorldPoint(mousePos)))
        {
            //SUMMON TARGET POINT ON SCREEN
            if (NavigationOn && activeModule == Modules.Navigation)
            {
                Vector3 worldPos;
                Ray ray = Camera.main.ScreenPointToRay(mousePos);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 1000f))
                {
                    worldPos = hit.point;
                }
                else
                {
                    worldPos = Camera.main.ScreenToWorldPoint(mousePos);
                }

                //SHIFT CLICK TO ADD MULTIPLE
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    CreateAndAddPoint(worldPos);
                }
                else
                {
                    ClearAllPoints();
                    CreateAndAddPoint(worldPos);
                }
            }
            else if(WeaponsOn && ammoLevel > 0 && activeModule == Modules.Weapons)
            {
                camera.TriggerShake(0.1f,0.1f);

                if (SoundManager.Instance != null) {
                    SoundManager.Instance.Play(SoundFX.TORPEDO_LAUNCH);
                }


                GameObject launchedObject = Object.Instantiate(torpedo, subTurret.transform.position+(subTurret.transform.right*0.5f), subTurret.transform.rotation*Quaternion.Euler(0,0,0));
                launchedObject.GetComponent<TorpedoBehaviour>().particleHolder = this.particleHolder;

                ammoLevel--;
            }

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Win"))
        {
            Debug.Log("Collision!");

            chip.GetComponent<PlugChip>().slotted = false;
            ModMan.activeModule = Modules.None;

            chip.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1500 + new Vector2(Random.Range(-500, 500), 0));


            //Stop the ship on impact
            EngineOn = false;
            NavigationOn = false;
            LifeSupportOn = false;
            WeaponsOn = false;
            ReloadOn = false;

            //Yeet
            ContactPoint2D[] contact = new ContactPoint2D[1];
            collision.GetContacts(contact);
            Vector3 dir = contact[0].point - new Vector2(transform.position.x, transform.position.y);
            // We then get the opposite (-Vector3) and normalize it
            dir = dir.normalized;
            Debug.Log(dir);
            world.transform.position += dir * 0.1f;

            camera.TriggerShake(0.2f, 0.5f);

            ClearAllPoints();
            shipSpeed = 0;
            hitsTaken++;

            if (SoundManager.Instance != null) {
                SoundManager.Instance.Play(SoundFX.CRASH);

                SoundManager.Instance.SetCrackVol(0.25f * (hitsTaken / 2.0f));
                SoundManager.Instance.PlayCrack();
            }


            if (collision.CompareTag("Enemy"))
            {
                Debug.Log("ENEMY HIT!");
                Destroy(collision.gameObject);
            }

        }
    }

    void CreateAndAddPoint(Vector3 position)
    {
        GameObject createdObject = Instantiate(targetPoint, new Vector3(position.x, position.y, 0), Quaternion.identity);
        targetList.Add(createdObject);
    }

    void ClearAllPoints()
    {
        foreach (GameObject obj in targetList)
        {
            Destroy(obj);
        }
        targetList.Clear();
    }

    void RunEngine(bool engineState)
    {
        //SPOOL UP ENGINE
        if (engineState && shipSpeed < maxSpeed && targetList.Count > 0)
        {
            shipSpeed += Time.deltaTime * acceleration;
            if (shipSpeed > maxSpeed)
                shipSpeed = maxSpeed;
        }
        else if (!engineState || targetList.Count == 0)
        {
            shipSpeed -= Time.deltaTime * deceleration;
            if (shipSpeed < 0)
                shipSpeed = 0;
        }
    }

    void RunNavigation(bool navigationState)
    {
        if(navigationState)
        {
            lineRend.enabled = true;
            lineRend.positionCount = targetList.Count + 1;
            lineRend.SetPosition(0, transform.position);
            for (int i = 0;i<targetList.Count;i++)
            {
                targetList[i].GetComponent<SpriteRenderer>().enabled = true;
                lineRend.SetPosition(i+1, targetList[i].transform.position);
            }
        }
        else
        {
            lineRend.enabled = false;
            foreach (GameObject obj in targetList)
            {
                obj.GetComponent<SpriteRenderer>().enabled = false;
            }
        }
    }

    void RunLifeSupport(bool lifeSupportState)
    {
        if (lifeSupportState && oxygenLevel < maxOxygen)
        {
            oxygenLevel += Time.deltaTime * oxygenRefillRate;
            if (oxygenLevel > maxOxygen)
                oxygenLevel = maxOxygen;
        }
        else if (!lifeSupportState)
        {
            oxygenLevel -= Time.deltaTime * oxygenDepletionRate;
            if (oxygenLevel < 0)
                oxygenLevel = 0;
        }

        if(oxygenLevel < 25)
        {
            oxygenWarningCounter += Time.deltaTime;
            if (oxygenWarningCounter >= 0.25)
            {
                oxygenWarningCounter = 0;
                OxygenLowIndicator.enabled = !OxygenLowIndicator.enabled;
                SoundManager.Instance.PlayEnemyBeep();
            }
        }
        else if (OxygenLowIndicator.enabled)
            OxygenLowIndicator.enabled = false;


        if(oxygenLevel == 0)
        {
            FadeInOut.Instance.sceneInt = 0;
            FadeInOut.Instance.isFadeIn = true;
        }
    }

    void UpdateIndicators()
    {
        OxygenIndicator.indicatorMax = maxOxygen;
        OxygenIndicator.indicatorValue = oxygenLevel;

        SpeedIndicator.indicatorMax = maxSpeed;
        SpeedIndicator.indicatorValue = shipSpeed;

        ReloadIndicator.indicatorMax = 2;
        ReloadIndicator.indicatorValue = reloadCounter;

        AmmoIndicator.indicatorValue = ammoLevel;
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.blue;

        Gizmos.DrawWireCube(transform.position, BoundingBox);
    }
}
