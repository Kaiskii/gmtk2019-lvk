﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineBehaviour : MonoBehaviour
{
    [SerializeField]float speed = 0.01f;
    [SerializeField] float triggerDistance = 2f;

    [SerializeField] Transform player;

    private void Start()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector2.Distance(transform.position,player.position) <= triggerDistance)
        {
            transform.right = player.position - transform.position;

            float step = speed * Time.deltaTime;
            transform.position += transform.right * step;
        }
    }
}
