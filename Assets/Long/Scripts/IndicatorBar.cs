﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorBar : MonoBehaviour
{
    [SerializeField] ModuleManager manager;
    [SerializeField] Modules activeModule;
    [SerializeField] Color activeColor;
    [SerializeField] Color usedColor;
    [SerializeField] Color disabledColor;

    [SerializeField] bool isPercentile = false;
    [SerializeField] List<SpriteRenderer> indicatorBars;

    public float indicatorValue = 0;
    public float indicatorMax = 0;
    [SerializeField] float value = 0;

    // Update is called once per frame
    void Update()
    {
        if (manager.activeModule == activeModule || activeModule == Modules.None)
        {
            if (!isPercentile)
            {
                for (int i = 0; i < indicatorBars.Count; i++)
                {
                    if (i <= indicatorValue - 1)
                        indicatorBars[i].color = activeColor;
                    else
                        indicatorBars[i].color = usedColor;
                }
            }
            else
            {
                value = Mathf.Round(indicatorValue * indicatorBars.Count / indicatorMax) ;
                for (int i = 0; i < indicatorBars.Count; i++)
                {
                    if (i <= value - 1)
                        indicatorBars[i].color = activeColor;
                    else
                        indicatorBars[i].color = usedColor;
                }
            }
        }
        else
        {
            for (int i = 0; i < indicatorBars.Count; i++)
            {
                    indicatorBars[i].color = disabledColor;
            }
        }
    }
}
