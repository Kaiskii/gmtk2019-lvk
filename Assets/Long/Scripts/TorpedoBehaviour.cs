﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorpedoBehaviour : MonoBehaviour
{
    SpriteRenderer render;
    [SerializeField] ParticleSystem spawnOnDeath;
    public GameObject particleHolder;

    [SerializeField] float speed = 1;
    [SerializeField] float timer = 0;

    private void Start()
    {
        render = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= 10)
        {
            Destroy(this.gameObject);
        }

        //Move forward
        float step = speed * Time.deltaTime;
        transform.position += transform.right * step;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Kill the enemy too
        if(collision.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
        }

        ParticleSystem spawned = Object.Instantiate(spawnOnDeath, transform.position, Quaternion.identity, particleHolder.transform);
        spawned.Play();

        SoundManager.Instance.Play(SoundFX.TORPEDO_LAUNCH);
        Destroy(this.gameObject);
    }
}
