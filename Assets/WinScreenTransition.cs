﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScreenTransition : MonoBehaviour
{
    [SerializeField]Typewriter top;
    [SerializeField] Typewriter middle;
    [SerializeField] Typewriter bottom;

    float counter;

    bool middlePlaying = false;
    bool bottomPlaying = false;

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;

        if(counter >= 0.5 && !middlePlaying)
        {
            middlePlaying = true;
            middle.Play = true;
        }
        else if(counter >= 1 && !bottomPlaying)
        {
            bottomPlaying = true;
            bottom.Play = true;
            top.Play = true;
        }
        else if (counter >= 6)
        {
            FadeInOut.Instance.isFadeIn = true;
            FadeInOut.Instance.sceneInt = 0;
        }
    }
}
