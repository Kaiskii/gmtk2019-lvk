﻿public enum Modules
{
    None = 0,
    LifeSupport,
    Information,
    Navigation,
    Weapons,
    Engine,
    Reload,
    Tutorial
}