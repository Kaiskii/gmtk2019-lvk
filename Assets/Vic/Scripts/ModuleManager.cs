﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleManager : MonoBehaviour
{
    [SerializeField] ShipBehaviour SubMainController;
    [SerializeField] CircularScan Scanner;
    private static ModuleManager _instance;
    public static ModuleManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    } // Singleton

    public Modules activeModule = Modules.None;

    public void ChangeActiveModule(Modules m)
    {
        activeModule = m;
    }

    void Update()
    {
        if (activeModule == Modules.Information)
        {
            if (!Scanner.scanActive)
                Scanner.scanActive = true;
        }
        else
        {
            if (Scanner.scanActive)
                Scanner.scanActive = false;
        }

        if (SubMainController.activeModule != activeModule)
        {
            SubMainController.activeModule = activeModule;
        }
    }
}
