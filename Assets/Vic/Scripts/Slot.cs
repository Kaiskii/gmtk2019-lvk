﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Slot : MonoBehaviour
{
    [SerializeField] Direction slotDirection = Direction.Up;
    bool moduleActive = false;
    [SerializeField] Modules module;
    [SerializeField] SpriteRenderer indicatorSR;
    [SerializeField] SpriteRenderer indicatorSL;

    public Direction GetSlotDirection()
    {
        return slotDirection;
    }
    public bool GetModuleActive()
    {
        return moduleActive;
    }
    public Modules GetModule()
    {
        return module;
    }
    public void SetIndicatorColor(Color c)
    {
        indicatorSR.color = c;
        indicatorSL.color = c;
    }

}
