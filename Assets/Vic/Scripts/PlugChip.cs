﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(BoxCollider2D))]

public class PlugChip : MonoBehaviour
{
    Vector3 screenPoint;
    Vector3 offset;
    public bool slotted = false;
    public bool held = false;
    Rigidbody2D rb2d;
    int gravScale = 3;
    [SerializeField] float followSpeed = 0.1f;
    [SerializeField] float yPlugOffset = 0.56f;
    GameObject slot;
    SpriteRenderer sr;
    Direction slottedDirection = Direction.None;
    

    void Start()
    {
        rb2d = this.GetComponent<Rigidbody2D>();
        sr = this.GetComponent<SpriteRenderer>();
    }

    void OnMouseDown()
    {
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        held = true;
    }

    void OnMouseUp()
    {
        held = false;
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        if (held&&slotted)
        {
            switch(slottedDirection)
            {
                case Direction.Up:
                    transform.position = new Vector3
                        (
                        transform.position.x,
                        Mathf.Lerp(transform.position.y, Mathf.Max(slot.transform.position.y, curPosition.y), followSpeed * Time.deltaTime),
                        transform.position.z
                    );
                    break;
                case Direction.Down:
                    transform.position = new Vector3
                        (
                        transform.position.x,
                        Mathf.Lerp(transform.position.y, Mathf.Min(slot.transform.position.y, curPosition.y), followSpeed * Time.deltaTime),
                        transform.position.z
                    );
                    break;
                case Direction.Left:
                    transform.position = new Vector3
                        (
                        Mathf.Lerp(transform.position.x, Mathf.Min(slot.transform.position.x, curPosition.x), followSpeed * Time.deltaTime),
                        transform.position.y,
                        transform.position.z
                    );
                    break;
                case Direction.Right:
                    transform.position = new Vector3
                        (
                        Mathf.Lerp(transform.position.x, Mathf.Max(slot.transform.position.x, curPosition.x), followSpeed * Time.deltaTime),
                        transform.position.y,
                        transform.position.z
                    );
                    break;
                case Direction.None:
                    transform.position = new Vector3
                        (
                        Mathf.Lerp(transform.position.x, curPosition.x, followSpeed),
                        Mathf.Lerp(transform.position.y, curPosition.y, followSpeed),
                        transform.position.z
                        );
                    break;
            }
                
        }
        else if(held)
        {
            rb2d.AddForce((curPosition - transform.position) * 100 - new Vector3(rb2d.velocity.x, rb2d.velocity.y, 0)*20);
        }
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Slot"))
        {
            slot = col.gameObject;
            if(held)
            {
                transform.position = new Vector3
                (
                col.transform.position.x,
                col.transform.position.y + yPlugOffset,
                -1
                );
                sr.sortingOrder = 4;
                slottedDirection = slot.GetComponent<Slot>().GetSlotDirection();
                slot.GetComponent<Slot>().SetIndicatorColor(Color.green);
                slotted = true;
                rb2d.gravityScale = 0;
                rb2d.velocity = new Vector2(0, 0);
                if(SoundManager.Instance!=null)
                {
                    SoundManager.Instance.PlayClick();
                }
                held = false;
                ModuleManager.Instance.ChangeActiveModule(slot.GetComponent<Slot>().GetModule());
            }
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if(slotted)
        {
            if (SoundManager.Instance != null)
            {
                SoundManager.Instance.Play(SoundFX.CLICK);
            }
            slotted = false;
            slot.GetComponent<Slot>().SetIndicatorColor(Color.red);
            sr.sortingOrder = 10;
            ModuleManager.Instance.ChangeActiveModule(Modules.None);
            transform.Translate(0, 0, -3);
        }
    }

    void Update()
    {
        if(slotted)
        {
            GetComponent<Rigidbody2D>().gravityScale = 0;
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
        else if (held)
        {
            GetComponent<Rigidbody2D>().gravityScale = 0;
        }
        else
        {
            GetComponent<Rigidbody2D>().gravityScale = gravScale;
        }
    }
}
