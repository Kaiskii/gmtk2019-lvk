﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundFX {
    CRASH = 0,
    CLICK,
    SONAR,
    TORPEDO_LAUNCH
}

public enum SoundBGM {
    BLACK_HEAT = 0,
    WARM_NIGHTS,
    OCEANIC_SPACE
}

public class SoundManager : MonoBehaviour {

    //Singleton Implementation
    private static SoundManager _instance;

    public static SoundManager Instance { get { return _instance; } }

    private void Awake() {
        if (_instance != null && _instance != this) {
            Destroy(this.gameObject);
        } else {
            DontDestroyOnLoad(this);
            _instance = this;
        }
    }

    //Variable Declarations
    public AudioSource SourceMusic;
    public AudioSource SourceFX;
    public AudioSource SourceBleep;
    public AudioSource SourceEnemy;
    public AudioSource SourceClick;
    public AudioSource SourceCrack;

    public List<AudioClip> AClipPool;

    public List<AudioClip> MusicClipPool;

    public AudioClip BleepText;

    //DEBUG
    int musicCounter = 0;


    // Play a single clip through the sound effects source.
    public void Play(SoundFX SF) {
        SourceFX.clip = AClipPool[(int) SF];
        SourceFX.Play();
    }

        public void Play(int i) {
        SourceFX.clip = AClipPool[i];
        SourceFX.Play();
    }

    // Play a single clip through the music source.
    public void PlayMusic(SoundBGM bgm) {
        SourceMusic.clip = MusicClipPool[(int) bgm];
        SourceMusic.Play();
    }

    public void PlayMusic(int bgm) {
        SourceMusic.clip = MusicClipPool[bgm];
        SourceMusic.Play();
    }

    public void PlayEnemyBeep() {
        SourceEnemy.Play();
    }

    public void PlayClick() {
        SourceClick.Play();
    }

    public void PlayBlip() {
        if(SourceBleep.clip == null) {
            SourceBleep.clip = BleepText;
        }

        SourceBleep.Play();
    }

    public void PlayCrack() {
        SourceCrack.Play();
    }

    public void SetCrackVol(float w) {
        SourceCrack.volume = w;
    }


    private void Update() {
        if (!SourceMusic.isPlaying) {
            if(musicCounter == MusicClipPool.Count - 1) {
                musicCounter = 0;
            }

            PlayMusic(musicCounter);
            musicCounter++;
        }
    }

}
