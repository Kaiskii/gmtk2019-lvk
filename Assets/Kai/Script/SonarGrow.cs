﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarGrow : MonoBehaviour {

    [SerializeField]
    float growthSpeed = 2.0f;

    [SerializeField]
    float fadeSpeed = 0.2f;

    // Update is called once per frame
    void Update() {
        transform.localScale += new Vector3(growthSpeed * Time.deltaTime, growthSpeed * Time.deltaTime);
        transform.GetComponent<SpriteRenderer>().color = new Color(0, 0.5f, 0, transform.GetComponent<SpriteRenderer>().color.a - fadeSpeed * Time.deltaTime);

        if(transform.localScale.x >= 1.5f) {
            transform.localScale = new Vector3(0, 0);
            transform.GetComponent<SpriteRenderer>().color = new Color(0, 0.5f, 0, 1.0f);
        }
    }
}
