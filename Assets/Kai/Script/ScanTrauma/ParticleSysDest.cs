﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSysDest : MonoBehaviour {
    // Start is called before the first frame update
    ParticleSystem ps;
    public CircularScan scanner;
    void Start() {
        ps = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update() {
        if (!ps.IsAlive() || !scanner.CheckInBoundingBox(transform.position)) {
            Destroy(this.gameObject);
        }
    }
}
