﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularScan : MonoBehaviour {

    public bool DEBUG_MODE = false;
    public bool scanActive = false;

    [SerializeField] float MAX_RAD = 5.0f;

    [SerializeField] float growthFact = 0.2f;

    [SerializeField] float c_Rad = 0.0f;

    [SerializeField] float MAX_LIFESPAN = 5.0f;

    [SerializeField] int polygonCount = 360;

    float innerAngle = 0.0f;

    Vector2 playerScanPos;

    public GameObject hitPS;
    public GameObject waterPS;
    public GameObject enemyPS;
    public Transform particleHolder;

    List<bool> hit = new List<bool>();

    //Calculate the Spread out of Circular Direction based on InnerAngle & i
    //Meant to use inside For Loop ONLY
    public Vector2 CircleDir(float i, float iA) {
        return new Vector2(Mathf.Sin(i * iA * Mathf.Deg2Rad), Mathf.Cos(i * iA * Mathf.Deg2Rad));
    }

    private void Start() {
        playerScanPos = transform.position;

        innerAngle = 360.0f / polygonCount;

        for (int i = 0; i < polygonCount; i++) {
            hit.Add(true);
        }
    }

    // Update is called once per frame
    void Update() {
        if (scanActive || DEBUG_MODE) {
            Scan();
        } else {
            c_Rad = 0;
            for(int i = 0; i < hit.Count; i++)
            {
                hit[i] = true;
            }

            playerScanPos = transform.position;
        }
    }

    void Scan() {

        //Raycasting
        for (int i = 0; i < polygonCount; i++) {

            RaycastHit2D res = Physics2D.Raycast(playerScanPos, CircleDir(i, innerAngle), c_Rad, LayerMask.GetMask("Terrain"));

            //Check if it hits something, the boolean of whether it hasn't hit anything to be true & in the bounding box
            if (res.collider != null && hit[i] == true && CheckInBoundingBox(res.point)) {
                hit[i] = false;

                //Configuring Duration & Lifespan of Particles
                float dur = MAX_LIFESPAN;

                //LONG MAKE ENEMY RED
                GameObject tempP;
                if (res.collider.CompareTag("Enemy")) {

                    if (SoundManager.Instance != null) {
                        SoundManager.Instance.PlayEnemyBeep();
                    }

                    tempP = Instantiate(enemyPS, new Vector3(res.point.x, res.point.y, 1), Quaternion.identity, particleHolder);
                } else {

                    tempP = Instantiate(hitPS, new Vector3(res.point.x, res.point.y, 1), Quaternion.identity, particleHolder);
                }

                var tm = tempP.GetComponent<ParticleSystem>().main;
                tm.duration = dur;
                tm.startLifetime = dur;
                tempP.GetComponent<ParticleSysDest>().scanner = this;
                tempP.GetComponent<ParticleSystem>().Play();
            }
            //LONG'S WATER SCAN
            else if(res.collider == null && CheckInBoundingBox(playerScanPos + CircleDir(i, innerAngle) * c_Rad) && Random.Range(0,20) == 0)
            {
                GameObject tempP = Instantiate(waterPS, playerScanPos+CircleDir(i, innerAngle)*c_Rad, Quaternion.identity, particleHolder);
                tempP.GetComponent<ParticleSysDest>().scanner = this;
                tempP.GetComponent<ParticleSystem>().Play();
            }
        }

        if (c_Rad >= MAX_RAD) {
            c_Rad = 0.0f;
        }

        //Reset Hit List & Update Position
        if (c_Rad <= 0.0f) {
            for (int i = 0; i < polygonCount; i++) {
                hit[i] = true;
            }
            

            if (SoundManager.Instance != null) {
                SoundManager.Instance.Play(SoundFX.SONAR);
            }

            playerScanPos = transform.position;
        }


        //Constantly Expanding current Radius
        c_Rad += growthFact * Time.deltaTime;

    }

    //Checks whether something is in the Bounding Box
    public bool CheckInBoundingBox(Vector2 objPos) {
        Vector2 subMain = transform.position;
        Vector2 BBox = GetComponent<ShipBehaviour>().BoundingBox;
        if(objPos.x >= subMain.x - (BBox.x / 2) && objPos.x <= subMain.x + (BBox.x / 2) && 
            objPos.y >= subMain.y - (BBox.y / 2) && objPos.y <= subMain.y + (BBox.y / 2)) {
            return true;
        }

        return false;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        for (int i = 0; i < polygonCount; i++) {
            Gizmos.DrawRay(playerScanPos, CircleDir(i, innerAngle) * c_Rad);
        }
    }
}
